#!/bin/ruby	

require 'openssl'
require 'socket'





#Ask for site name
puts "Hello! Give me a website and I will try to connect on port 443:"

#Get sitename
site_name = gets.chomp

#Specify to only try TLS 1.1 and 1.2 with context
ctx = OpenSSL::SSL::SSLContext.new
ctx.min_version = OpenSSL::SSL::TLS1_1_VERSION
ctx.max_version = OpenSSL::SSL::TLS1_2_VERSION

tcp_client = TCPSocket.new(site_name, 443)
ssl_client = OpenSSL::SSL::SSLSocket.new(tcp_client)
ssl_client.hostname = site_name
ssl_client.sync_close = true
ssl_client.connect

#ssl_client.puts "hello server!"

cert = ssl_client.peer_cert
puts cert.version
puts cert.subject.to_s

puts ssl_client.state


#puts ssl_client.gets


ssl_client.close


